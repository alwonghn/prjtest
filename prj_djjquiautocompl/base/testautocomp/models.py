from django.db import models


class Drug(models.Model):
    rxcui = models.IntegerField()
    short_name = models.CharField(max_length=50)
    is_brand = models.IntegerField(max_length=1)
