from django.shortcuts import render
import json
from django.http.response import HttpResponse

import csv
from testautocomp.models import Drug

def load_drugs(file_path):
    "this loads drugs from pipe delimited file with headers"

    print("into view load_drugs")

    reader = csv.DictReader(open(file_path))
    for row in reader:
        drug = Drug(rxcui=row['Rxcui'], short_name=row['Short Name'], is_brand=row['Is Brand'])
        drug.save()

def get_drugs(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        drugs = Drug.objects.filter(short_name__icontains=q)[:20]
        results = []
        for drug in drugs:
            drug_json = {}
            drug_json['id'] = drug.rxcui
            drug_json['label'] = drug.short_name
            drug_json['value'] = drug.short_name
            results.append(drug_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)
