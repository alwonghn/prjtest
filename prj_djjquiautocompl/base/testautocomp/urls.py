from django.conf.urls import url
from .views import get_drugs

urlpatterns = [
    url(r'^index/', get_drugs, name='get_drugs')
]