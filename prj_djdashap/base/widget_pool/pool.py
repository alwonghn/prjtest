"""AWS: 


THIS MODULE WILL NOW ONLY ACT TO 
1. DEFINE THE WIDEGET POOL
2. AUTO INSTANTIATE IT AT STARTUP


A: FOR 2. THE INIT METHOD FROM HERE
WAS TRANSFERRED TO THE APP INIT METHOD

B: THE discover_widgets method containing
DJANGO LOAD CALL WILL BE TRANSFERRED TO
THE DASHBOARD MANAGER APP

"""


"""Widget Pool for the dashboard_app."""
from django.core.exceptions import ImproperlyConfigured
from django_load.core import load

from widget_pool.exceptions import WidgetAlreadyRegistered
from widget_pool.widget_base import DashboardWidgetBase


class DashboardWidgetPool(object):
    """
    A pool of registered DashboardWidgets.

    This class should only be instantiated at the end of this file, therefore
    serving as a singleton. All other files should just import the instance
    created in this file.

    Inspired by
    https://github.com/divio/django-cms/blob/develop/cms/plugin_pool.py

    """
    def __init__(self):
        self.widgets = {}
        self.discovered = False
        print("INTO WIDGET_POOL/POOL.PY/class DASHBOARDWIDGETPOOL/INIT")




    # def register_widget(self, widget_cls, **widget_kwargs):
    # def register_widget(self, widget_cls, positon=None):
    def register_widget(self, widget):
        """
        Registers the given widget.

        Widgets must inherit ``DashboardWidgetBase`` and you cannot register
        the same widget twice.

        :widget_cls: A class that inherits ``DashboardWidgetBase``.

        """
        # if not issubclass(widget_cls, DashboardWidgetBase):
        #     raise ImproperlyConfigured(
        #         'DashboardWidgets must be subclasses of DashboardWidgetBase,'
        #         ' {0} is not.'.format(widget_cls))

        # widget = widget_cls(**widget_kwargs)
        # widget = widget_cls(position)
        print('type of widget= {0}'.format(type(widget)))
        widget_name = widget.get_name()
        print('got widget name={0}'.format(widget_name))
        if widget_name in self.widgets:
            raise WidgetAlreadyRegistered(
                'Cannot register {0}, a widget with this name {1} is already '
                'registered.'.format(widget_name, widget_name))
            # 'registered.'.format(widget_cls, widget_name))

        self.widgets[widget_name] = widget
        print('new length of widgets[]={0}'.format(len(self.widgets)))

    # def unregister_widget(self, widget_cls):
    #     """Unregisters the given widget."""
    #     if widget_cls.__name__ in self.widgets:
    #         del self.widgets[widget_cls().get_name()]





# wierd - django does not instantiate this class and go into its init unless singleton is made here
print('before DashboardWidgetPool instantiated as singleton')
widget_pool_singleton = DashboardWidgetPool()
print('after DashboardWidgetPool instantiated as singleton')