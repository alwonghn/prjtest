from django.apps import AppConfig
# from . pool import DashboardWidgetPool


class WidgetPoolConfig(AppConfig):
    # aws: this must be the name of the app directory/module/app name/already created directory
    name = 'widget_pool'

    def ready(self):
        print("INTO WIDGET POOL APPS PY WIDGETPOOLCONFIG READY")
        # dashboard_widget_pool = DashboardWidgetPool()