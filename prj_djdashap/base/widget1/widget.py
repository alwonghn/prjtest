from django.shortcuts import render

# """Widgets for the ACME project."""
# from dashboard_manager import dashboard_widget_pool
#
# from widget1 import dashboard_widgets as widgets
#
# """Widgets for the dashboard_acme_users app."""
from widget_pool.widget_base import DashboardWidgetBase
from widget_pool.pool import widget_pool_singleton

# class Widget1(object):
class Widget1(DashboardWidgetBase):
    """Displays the total amount of users currently in the database."""
    template_name = 'widget1/widgets/widget1.html'

    def __init__(self):
        print("INTO WIDGET1/WIDGET.PY/class WIDGET1/INIT")

    def get_context_data(self, kwargs=None):
        # ctx = super(Widget1, self).get_context_data()
        # count = User.objects.all().count()
        # ctx.update({'value': count, })
        # return ctx

        context = super(Widget1, self).get_context_data(**kwargs)


        # context['latest_articles'] = Article.objects.all()[:5]

        # aws: context is a dict-like of lists(each list is a table) of dicts(each dict is a row of columns)
        data_list = [
            {'col1': 'testerapp3-datalist-dictrow1: col1 row1', 'col2': 'testerapp3-datalist-dictrow1: col2 row1'},
            {'col1': 'testerapp3-datalist-dictrow2: col1 row2', 'col2': 'testerapp3-datalist-dictrow2: col2 row2', }
        ]
        context = {'data_list': data_list, }
        return context

print('before Widget1 instantiated as singleton')
widget_1_singleton = Widget1()
print('after Widget1 instantiated as singleton')

# dashboard_widget_pool.register_widget(DummyWidget, position=1)
# dashboard_widget_pool.register_widget(widgets.Widget1, position=1)
widget_pool_singleton.register_widget(widget_1_singleton)
print('after Widget1 registered in pool')