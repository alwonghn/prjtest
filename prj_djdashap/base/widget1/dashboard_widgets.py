# """Widgets for the ACME project."""
# from dashboard_manager import dashboard_widget_pool
#
# from widget1 import dashboard_widgets as widgets
#
# """Widgets for the dashboard_acme_users app."""
# from widget_pool.widget_base import DashboardWidgetBase
#
#
# class Widget1(DashboardWidgetBase):
#     """Displays the total amount of users currently in the database."""
#     template_name = 'widget1/widgets/widget1.html'
#
#     def get_context_data(self):
#         # ctx = super(Widget1, self).get_context_data()
#         # count = User.objects.all().count()
#         # ctx.update({'value': count, })
#         # return ctx
#
#         context = super(Widget1, self).get_context_data(**kwargs)
#         # context['latest_articles'] = Article.objects.all()[:5]
#
#         # aws: context is a dict-like of lists(each list is a table) of dicts(each dict is a row of columns)
#         data_list = [
#             {'col1': 'testerapp3-datalist-dictrow1: col1 row1', 'col2': 'testerapp3-datalist-dictrow1: col2 row1'},
#             {'col1': 'testerapp3-datalist-dictrow2: col1 row2', 'col2': 'testerapp3-datalist-dictrow2: col2 row2', }
#         ]
#         context = {'data_list': data_list, }
#         return context
#
#
#
#
# # dashboard_widget_pool.register_widget(DummyWidget, position=1)
# dashboard_widget_pool.register_widget(widgets.Widget1, position=1)
