"""Widgets for the ACME project."""
from dashboard_manager import dashboard_widget_pool

from widget2 import dashboard_widgets as widgets

"""Widgets for the dashboard_acme_users app."""
from widget_pool.widget_base import DashboardWidgetBase

from django.contrib.auth.models import User


class Widget2(DashboardWidgetBase):
    """Displays the total amount of users currently in the database."""
    template_name = 'widget2/widgets/widget2.html'

    def get_context_data(self):
        ctx = super(Widget2, self).get_context_data()
        count = User.objects.all().count()
        ctx.update({'value': count, })
        return ctx

# dashboard_widget_pool.register_widget(DummyWidget, position=1)
dashboard_widget_pool.register_widget(widgets.Widget2, position=2)