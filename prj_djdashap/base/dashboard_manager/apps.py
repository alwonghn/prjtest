from django.apps import AppConfig


class WidgetManagerConfig(AppConfig):
    name = 'dashboard_manager'
