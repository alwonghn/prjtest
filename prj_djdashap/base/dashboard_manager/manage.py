

class DashboardWidgetManager(object):

    def __init__(self):
        print("INTO DASHBOARD_MANAGER/MANAGE.PY/class DASHBOARDWIDGETMANAGER/INIT")


# def discover_widgets(self):
#     """
#     Searches for widgets in all INSTALLED_APPS.
#
#     This will be called when you call ``get_all_widgets`` for the first
#     time.
#
#     """
#     if self.discovered:
#         return
#     load('dashboard_widgets')
#     self.discovered = True
#     # added on 171121
#     return
#
# def get_widgets(self):
#     """Discovers all widgets and returns them."""
#     self.discover_widgets()
#     return self.widgets
#
# def get_widgets_sorted(self):
#     """Returns the widgets sorted by position."""
#     result = []
#     for widget_name, widget in self.get_widgets().items():
#         result.append((widget_name, widget, widget.position))
#     result.sort(key=lambda x: x[2])
#     return result
#
# def get_widget(self, widget_name):
#     """Returns the widget that matches the given widget name."""
#     return self.widgets[widget_name]
#
# def get_widgets_that_need_update(self):
#     """
#     Returns all widgets that need an update.
#
#     This should be scheduled every minute via crontab.
#
#     """
#     result = []
#     for widget_name, widget in self.get_widgets().items():
#         if widget.should_update():
#             result.append(widget)
#     return result
#

print('before DashboardWidgetManager instantiated as singleton')
widget_manager_singleton = DashboardWidgetManager()
print('after DashboardWidgetManager instantiated as singleton')