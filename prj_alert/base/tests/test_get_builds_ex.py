from django.core.management import call_command
from django.test import TestCase
from django.utils.six import StringIO

class CommandTest(TestCase):
    def test_command_output(self):
        try:
            out = StringIO()
            call_command('get_builds', stdout=out)
        except Exception as e:
            print(e)
        finally:
            self.assertIn('Expected output', out.getvalue())