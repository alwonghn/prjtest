
from sentry.models import Alert

try:
    alerts = []
    for alert in Alert.objects.all().order_by('alert_count'):
        most_counts = alert.build_set.all().first()
        alert_details = {
            'count': alert.alert_count,
            'level': alert.alert_level,
            'cause': alert.alert_cause,
            'last_seen':alert.alert_last_seen
        }
        alerts.append(alert_details)

except Exception as e:
    # This connection to TeamCity can fail but can just be retried next time
    print(e)

else:
    print(alerts)
