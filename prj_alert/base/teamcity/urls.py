from django.conf.urls import url
from teamcity.views import ProjectsOverview

urlpatterns = [
    url(r'^$', ProjectsOverview.as_view()),
]