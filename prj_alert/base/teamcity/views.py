from django.shortcuts import render

from django.views.generic import TemplateView
from sentry.management.commands.get_builds import Command

from sentry.models import Alert


class ProjectsOverview(TemplateView):
    template_name = 'teamcity-dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print('in ProjectsOverview before call Command')
        try:
            Command().handle()
        except Exception as e:
            # This connection to TeamCity can fail but can just be retried next time
            pass
        print('in ProjectsOverview after call Command, before making alerts list for context')
        try:
            alerts = []
            for alert in Alert.objects.all().order_by('-alert_count'):
                # most_counts = alert.build_set.all().first()
                alert_details = {
                    'count_level': '{0} {1} alerts from error: "{2}"'.format(alert.alert_count, alert.alert_level, alert.alert_cause),
                    'description': 'reason: "{0}"'.format(alert.alert_description_1),
                    'last_seen': 'last occurrence at {0}'.format(alert.alert_last_seen),
                }
                alerts.append(alert_details)

        except Exception as e:
            # This connection to TeamCity can fail but can just be retried next time
            print(e)

        else:
            context.update({
                'alerts' : alerts
            })
            print('after updated context with alerts list of lenght {0}'.format(len(alerts)))
            print(alerts)
            return context
