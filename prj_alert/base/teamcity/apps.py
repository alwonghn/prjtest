from django.apps import AppConfig


class TeamcityConfig(AppConfig):
    name = 'teamcity'
