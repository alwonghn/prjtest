from django.db import models

class Alert(models.Model):
    alert_id = models.CharField(max_length=10)
    alert_level = models.CharField(max_length=20)
    alert_count = models.IntegerField()
    alert_description_1 = models.CharField(max_length=100)
    alert_description_2 = models.CharField(max_length=100)
    alert_cause = models.CharField(max_length=100)
    alert_last_seen = models.DateTimeField()
    alert_link = models.CharField(max_length=100)

    # def __str__(self):
    #     return '{name}: Build {id} - {status}. Tests: {tests} (Passed: {passed}, Failed: {failed}, Skipped: {skipped})'.format(
    #         name=self.project.name,
    #         id=self.build_number,
    #         status=self.build_status,
    #         tests=self.test_count,
    #         passed=self.passed_tests,
    #         failed=self.failed_tests,
    #         skipped=self.skipped_tests
    #     )

    # class Meta:
    #     ordering = ('-build_number',)


# class Lookup_Project_Names(models.Model):
#     project_name_dashboard = models.CharField(max_length=20)
#     project_name_rhodecode = models.CharField(max_length=20)
#     project_name_sentry = models.CharField(max_length=20)
#     project_name_teamcity = models.CharField(max_length=20)
#     project_name_agent = models.CharField(max_length=20)
#     project_name_paddy = models.CharField(max_length=20)
