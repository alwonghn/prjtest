from django.core.management import BaseCommand
import requests
import json
from sentry.models import Alert
from collections import OrderedDict

class Command(BaseCommand):

    def handle(self, *args, **options):
        headers = {'Authorization': 'Bearer e71c09aef20740429498f0146541a9d1a2989a33d57144cab2f44abe41aa21e5'}
        alerts_url = 'https://sentry.its.otago.ac.nz/api/0/projects/its-applications/rod-test/issues/?query=is%3Aunresolved&limit=25&sort=date&statsPeriod=24h&shortIdLookup=1'

        try:
            response = requests.get(url=alerts_url, headers=headers)
            issues_list = json.loads(response.text)
            list_fatals = [issue for issue in issues_list if issue.get('level') == 'fatal']
            print("length of list fatals {0}".format(len(list_fatals)))
            report_list = []
            for error in list_fatals:
                odict = OrderedDict()
                odict['shortId']=error.get('shortId')
                odict['level']=error.get('level')
                odict['count']=error.get('count')
                odict['metadatatitle']=error.get('metadata').get('title')
                odict['title']=error.get('title')
                odict['culprit']=error.get('culprit')
                odict['lastseen']=error.get('lastSeen')
                odict['permalink']=error.get('permalink')
                # report_odict = OrderedDict(
                #     {'shortId': error.get('shortId')},
                #     {'level': error.get('level')},
                #     {'count': error.get('count')},
                #     {'metadatatitle': error.get('metadata').get('title')},
                #     {'title': error.get('title')},
                #     {'culprit': error.get('culprit')},
                #     {'lastseen': error.get('lastSeen')},
                #     {'permalink': error.get('permalink')},
                # )
                report_list.append(odict)

        except Exception as e:
            print(e)
            raise
        else:
            print('completed making {0} report_list-list len {1}'.format(type(report_list),len(report_list)))

            # clear last alerts before inserting freshly obtained alerts list
            Alert.objects.all().delete()
            # alert.objects.truncate()
            count = 0
            for error in report_list:
                count += 1
                try:
                    alert = Alert()
                    alert.alert_id=error.get('shortId')
                    alert.alert_level = error.get('level')
                    alert.alert_count=error.get('count')
                    alert.alert_description_1=error.get('metadatatitle')
                    alert.alert_description_2=error.get('title')
                    alert.alert_cause=error.get('culprit')
                    alert.alert_last_seen=error.get('lastseen')
                    alert.alert_link=error.get('permalink')
                    alert.save()
                except Exception as e:
                    print(e)
                    raise
            print('completed inserting {0} records into db'.format(count))


    # def load_rest_page(self, rest_url):
    #     try:
    #         headers = {'Authorization':'Bearer e71c09aef20740429498f0146541a9d1a2989a33d57144cab2f44abe41aa21e5'}
    #         response = requests.get(url=rest_url, headers=headers)
    #         data = json.loads(response.text)
    #     except Exception as e:
    #         raise
    #     else:
    #         return data