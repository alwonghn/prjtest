# -*- encoding: utf-8 -*-
# from dashing.widgets import Widget
from dashing.widgets import NumberWidget

class customwidget(NumberWidget):
    title = 'customwidget title'
    more_info = 'customwidget more_info'
    updated_at = 'customwidget updated_at'
    data = []

    def get_title(self):
        return self.title

    def get_more_info(self):
        return self.more_info

    def get_updated_at(self):
        return self.updated_at

    def get_data(self):
        return self.data

    def get_context(self):
        return {
            'title': self.get_title(),
            'moreInfo': self.get_more_info(),
            'updatedAt': self.get_updated_at(),
            'data': self.get_data(),
        }