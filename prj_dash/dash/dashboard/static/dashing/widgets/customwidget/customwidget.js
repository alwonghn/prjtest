/*NEW 171117*/
// Dashing.widgets.List = function (dashboard) {
//     var self = this,
//         widget;
//     this.__init__ = Dashing.utils.widgetInit(dashboard, 'list');
//     this.row = 2;
//     this.col = 1;
//     this.scope = {};
//     this.getWidget = function () {
//         return widget;
//     };
//     this.getData = function () {};
//     this.interval = 10000;
// };

/* global Dashing */

Dashing.widgets.Number = function(dashboard) {
    var self = this;
    self.__init__ = Dashing.utils.widgetInit(dashboard, 'customwidget');
    self.row = 1;
    self.col = 1;
    self.color = '#96bf48';
    self.scope = {};
    self.getWidget = function () {
        return this.__widget__;
    };
    self.getData = function () {};
    self.interval = 1000;
};