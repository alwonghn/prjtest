import hashlib
import hmac

key = 'dummykey'
http_method = 'GET'
host_domain = 'otagouni-00.vaultgrc.com'
request_uri = '/api/'
date = ''
context = ''

str_to_be_hashed = '{0}{1}{2}{3}{4}{5}'.format(key,http_method,host_domain,request_uri,date,context)

#generate the hash
signature = hmac.new(
    key,
    str_to_be_hashed,
    hashlib.sha256
).hexdigest()