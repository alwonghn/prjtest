import json
from main.loadpage import PageLoader
import requests

alerts_url1 = 'https://sentry.its.otago.ac.nz/its-applications/rod-production'
alerts_url2 = 'https://sentry.its.otago.ac.nz/api/0/projects/its-applications/rod-test/issues/?query=is%3Aunresolved&limit=25&sort=date&statsPeriod=24h&shortIdLookup=1'
# alerts_url2 = 'https://sentry.its.otago.ac.nz/api/0/projects/its-applications/rod-production/issues/?query=is%3Aunresolved&limit=25&sort=date&statsPeriod=24h&shortIdLookup=1'

# try:
#     PageLoader().go_to_landing(alerts_url1)
# except Exception as e:
#     print(e)
# else:
#     try:
#         data = PageLoader().load_rest_page(alerts_url2)
#         print(data)
#     except Exception as e2:
#         print(e2)

try:
    report_list = PageLoader().load_page_as_list_filter_for_fatals(alerts_url2)
    print(report_list)
except Exception as e2:
    print(e2)
