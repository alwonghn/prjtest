import requests
import json


class PageLoader:

    def go_to_landing(self, page_url):
        try:
            headers = {'Accept': 'application/json'}
            user = 'its-applications@otago.ac.nz'
            password = '1vDRhy95456ghys'
            auth = (user, password)
            requests.get(url=page_url, headers=headers, auth=auth)
        except Exception as e:
            raise


    def load_page_as_list(self, page_url):
        try:
            headers = {'Accept': 'application/json'}
            user = 'its-applications@otago.ac.nz'
            password = '1vDRhy95456ghys'
            auth = (user, password)
            response = requests.get(url=page_url, headers=headers, auth=auth)
            data = json.loads(response.text)
            pass
        except Exception as e:
            print(e)
            raise
        else:
            return data


    def load_rest_page_as_list(self, rest_url):
        try:
            headers = {'Authorization':'Bearer e71c09aef20740429498f0146541a9d1a2989a33d57144cab2f44abe41aa21e5'}
            response = requests.get(url=rest_url, headers=headers)
            response_data = json.loads(response.text)
            print("type of response_data is {0}".format(type(response_data)))
            print("LENGTH of {0} is {1}".format(type(response_data),len(response_data)))
            typeof_first_element_of_list = type(response_data[0])
            print("type of first element of response_data list is {0}".format(typeof_first_element_of_list))
        except Exception as e:
            raise
        else:
            return response_data
            # return response_data[0]

    def load_rest_page_as_list_return_list_tuples(self, rest_url):

        try:
            headers = {'Authorization':'Bearer e71c09aef20740429498f0146541a9d1a2989a33d57144cab2f44abe41aa21e5'}
            response = requests.get(url=rest_url, headers=headers)
            response_data = json.loads(response.text)
            print("type of response_data is {0}".format(type(response_data)))
        except Exception as e:
            raise
        else:
            return response_data


    def load_page_as_list_filter_for_fatals(self, rest_url):
        try:
            headers = {'Authorization':'Bearer e71c09aef20740429498f0146541a9d1a2989a33d57144cab2f44abe41aa21e5'}
            response = requests.get(url=rest_url, headers=headers)
            response_data = json.loads(response.text)
            print("type of response_data is {0}".format(type(response_data)))
            print("LENGTH of {0} is {1}".format(type(response_data),len(response_data)))
            typeof_first_element_of_list = type(response_data[0])
            print("type of first element of response_data list is {0}".format(typeof_first_element_of_list))
            list_fatals = [issue for issue in response_data if issue.get('level') == 'fatal']
            print("length of list fatals {0}".format(len(list_fatals)))
        except Exception as e:
            raise
        else:
            return response_data



    # def load_file_as_string(self, file_name):
    #     with open(file_name) as json_file:
    #         #
    #         json_data = json_file.read()
    #         json_string = json.loads(self, json_data)
    #         print("type of json_string is {0}".format(type(json_string)))
    #         return json_string

#
# class GetAlertsFromPage:
#
#     alerts_url = 'https://sentry.its.otago.ac.nz/api/0/projects/its-applications/rod-test/issues/?query=is%3Aunresolved&limit=25&sort=date&statsPeriod=24h&shortIdLookup=1'
#
#     def load_page_as_list(self):
#         pass