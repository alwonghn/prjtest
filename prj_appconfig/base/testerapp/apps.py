from django.apps import AppConfig


class TesterappConfig(AppConfig):
    name = 'testerapp'

# https://stackoverflow.com/questions/37429726/overriding-appconfig-ready
# method 1 : init.py of app default_app_config = testerapp.apps.TesterAppConfig
# method 2 : in settings installed apps: testerapp.apps.TesterAppConfig

    def ready(self):
        print('2 into testerapp apps.py TesterappConfig ready')
