from django.db import models

class Document(models.Model):

    file_name = models.CharField(max_length=50, blank=True)
    storage_path =  models.CharField(max_length=255, blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        get_latest_by = 'uploaded_at'

# docfile = models.FileField(upload_to='documents/')
# description = models.CharField(max_length=255, blank=True)
# docfile = models.FileField(upload_to='documents/%Y/%m/%d')