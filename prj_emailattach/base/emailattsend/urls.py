
from django.conf.urls import url
# from . views import UploadFileView
from . views import Home,SimpleUploadView, send_email


urlpatterns = [
    url(r'^home/$', Home, name='home'),
    # url(r'^emailattsend/',UploadFileView, name='upload_file_view'),
    url(r'^uploads/simple/$', SimpleUploadView, name='simple_upload'),
    url(r'^email/send/$',send_email, name='send_email' ),
]