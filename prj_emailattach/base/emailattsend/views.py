from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy


from django.views.generic.edit import FormView
from .forms import UploadFileForm
from .models import Document

from django.core.files.storage import FileSystemStorage
from django.core.mail import EmailMessage

def Home(request):
    documents = Document.objects.all()
    # return render(request, 'emailattsend/home.html', { 'documents': documents })

    return render(request, 'emailattsend/create_email.html', { 'documents': documents })


def SimpleUploadView(request):
    if request.method == 'POST' and request.FILES['file']:
        myfile = request.FILES['file']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        print('SIMPLEUPLOADVIEW filename={0}'.format(filename))
        uploaded_file_url = fs.url(filename)
        # added
        doc_record = Document(file_name=myfile.name, storage_path=uploaded_file_url)
        doc_record.save()
        uploaded_file_pk = doc_record.pk
        print('SIMPLEUPLOADVIEW upload file pk={0}'.format(uploaded_file_pk))
        # uploaded_file = Document.objects.get(pk=uploaded_file_pk)
        # return render(request, 'emailattsend/create_email.html', {
        #     'uploaded_file_url': uploaded_file_url,
        #     'uploaded_file_pk' : uploaded_file_pk,
        # })
        documents =  Document.objects.filter(pk=uploaded_file_pk)
        print('SIMPLEUPLOADVIEW type of documents {0}'.format(type(documents)))

        return render(request, 'emailattsend/create_email.html', { 'documents': documents })

    else:
        # return render(request, 'emailattsend/simple_upload.html')
        return render(request, 'emailattsend/create_email.html')



# def handle_uploaded_file(file):
#     with open('tmp/uploads/file.txt') as destination:
#         for chunk in file.chunks():
#             destination.write(chunk)



# def UploadFileView(request):
#     if request.method == 'POST':
#         form = UploadFileForm(request.POST, request.FILES)
#         if form.is_valid():
#             # handle_uploaded_file(request.FILES['file'])
#             document = Document(docfile=request.FILES('file'))
#             document.save()
#             return HttpResponseRedirect(reverse_lazy('upload_file_view'))
#     else:
#         form = UploadFileForm()
#     return render(request, 'emailattsend/uploadandemail.html', {'form': form})






# class FileFieldView(FormView):
#     form_class = FileFieldForm
#     template_name = 'upload.html'  # Replace with your template.
#     success_url = '...'  # Replace with your URL or reverse().
#
#     def post(self, request, *args, **kwargs):
#         form_class = self.get_form_class()
#         form = self.get_form(form_class)
#         files = request.FILES('file')
#         if form.is_valid():
#             for f in files:
#                 ...  # Do something with each file.
#             return self.form_valid(form)
#         else:
#             return self.form_invalid(form)



def send_email(request):
    email = EmailMessage()
    email.subject = 'Hello'
    email.body = 'Body goes here'
    email.from_email = 'MyEmail@MyEmail.com'
    email.to = 'alfred.wong@otago.ac.nz'
    email.reply_to = 'alfred.wong@otago.ac.nz'
    email.headers = {'(email.headers)Message-ID': 'foo'}
    email.attach_file(Document.objects.latest('uploaded_at'))
    email.send()
    Document.objects.all().delete()