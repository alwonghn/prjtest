"""Widgets for the ACME project."""
from dashboard_app.dashboard_widgets import DummyWidget
from dashap1 import dashboard_widgets as widgets
from dashboard_app.widget_pool import dashboard_widget_pool
"""Widgets for the dashboard_acme_users app."""
from dashboard_app.widget_base import DashboardWidgetBase

from django.contrib.auth.models import User


class UserCountWidget(DashboardWidgetBase):
    """Displays the total amount of users currently in the database."""
    template_name = 'dashap1/widgets/user_count.html'

    def get_context_data(self):
        ctx = super(UserCountWidget, self).get_context_data()
        count = User.objects.all().count()
        ctx.update({'value': count, })
        return ctx

# dashboard_widget_pool.register_widget(DummyWidget, position=1)
dashboard_widget_pool.register_widget(widgets.UserCountWidget, position=1)
