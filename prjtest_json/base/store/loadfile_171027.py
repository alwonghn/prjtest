import json

class FileLoader:

    def load_file_as_list(self, file_name):
        with open(file_name) as json_file:
            # json.load returns a python list!
            json_list = json.load(json_file)
            print("type of json_list is {0}".format(type(json_list)))
            return json_list

    def load_file_as_string(self, file_name):
        with open(file_name) as json_file:
            #
            json_data = json_file.read()
            json_string = json.loads(self, json_data)
            print("type of json_string is {0}".format(type(json_string)))
            return json_string


class GetAlertsFromPage:

    alerts_url = 'https://sentry.its.otago.ac.nz/api/0/projects/its-applications/rod-test/issues/?query=is%3Aunresolved&limit=25&sort=date&statsPeriod=24h&shortIdLookup=1'

    def load_page_as_list(self):
        pass
