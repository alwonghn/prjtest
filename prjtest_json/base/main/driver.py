import json
from main.loadfile import FileLoader
import requests

report_list = []
try:
    data_file_list = FileLoader().load_file_as_list('issuespgqueryresult.json')
    list_fatals = [obj for obj in data_file_list if obj.get('level') == 'fatal']


    for obj in list_fatals:
        report_tup = (
            {'shortId':obj.get('shortId')},
            {'level': obj.get('level')},
            {'count' : obj.get('count')},
            {'metadatatitle': obj.get('metadata').get('title')},
            {'title': obj.get('title')},
            {'culprit': obj.get('culprit')},
            {'lastseen': obj.get('lastSeen')},
            {'permalink': obj.get('permalink')},
        )
        report_list.append(report_tup)
except Exception as e:
    print(e)
    raise
else:
    print('report_list=', report_list)
    print('report_list len=', len(report_list))
    print('first error=', report_list[0])

