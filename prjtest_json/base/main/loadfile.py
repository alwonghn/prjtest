import json

class FileLoader:

    def load_file_as_list(self, file_name):
        with open(file_name) as json_file:
            # json.load returns a python list!
            json_list = json.load(json_file)
            print("type of json_list is {0}".format(type(json_list)))
            return json_list


