from django.db import models


class AddressMixin(models.Model):

    attention_line = models.CharField(max_length=40, blank=True, null=True, verbose_name='Attention line (40 characters max)')
    address_line_one = models.CharField(max_length=40, blank=True, null=True, verbose_name='Address line one (40 characters max)')
    address_line_two = models.CharField(max_length=40, blank=True, null=True, verbose_name='Address line two (40 characters max)')

    class Meta:
        abstract = True