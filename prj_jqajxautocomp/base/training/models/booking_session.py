from django.db import models
from django.db.models import CASCADE
# from otago_oracle.models import Audit
# from training.engine.booking.logic import BookingSessionQuerySet


# class BookingSession(Audit):
#
#     booking = models.ForeignKey('training.Booking', on_delete=CASCADE)
#     session = models.ForeignKey('training.Session', on_delete=CASCADE)
#     attended = models.BooleanField(default=False)
#     apologies = models.BooleanField(default=False)  # _didn't_ attend, _did_ phone/email to say "sorry, can't make it"
#
#     @property
#     def attended_or_apologies(self):
#         if self.attended:
#             return "Yes"
#         if self.apologies:
#             return "Apologies"
#         return ""
#
#     def __str__(self):
#         return 'Booking Session for {0} : {1}'.format(self.booking, self.session,)
#
#     class Meta:
#         app_label = 'training'
#         verbose_name = 'Booking Session'
#
#     objects = BookingSessionQuerySet.as_manager()
