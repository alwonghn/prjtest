from django.core.validators import validate_email, MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import PROTECT
from model_utils import Choices
# from otago_hr.models import PersonJobsDatesInDepartments
# from otago_oracle.models import Audit
# from training.engine.booking.logic import BookingQuerySet
    #, CreditCardPaymentQuerySet
#
# from training.models import statuses
from training.models.address import AddressMixin
# from training.models.statuses import BOOKING_STATUS, KLASS_STATUS_COMPLETED

BOOKING_PUBLIC_PERSON_CLASS = 'PUBLIC'



class User(models.Model):
    username = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)



# class Booking(Audit, AddressMixin):
class Booking(AddressMixin):
    KIND = Choices(('invoice', 'Invoice'), ('cash', 'Cash Payment'), ('credit_card', 'Credit Card Payment'))
    TYPE = Choices(('STAFF', 'Staff'), ('STUDENT', 'Student'), ('OTHER', 'Non-university User'))
    HOW_HEARD_ABOUT = Choices(
        ('COWORKER', 'Co-worker'),
        ('MANAGER', 'Manager'),
        ('EMAIL', 'Email'),
        ('SEARCH_ENGINE', 'Google Search'),
        ('NEWSLETTER', 'Newsletter'),
        ('GUIDE', 'Training Guide'),
        ('WEBSITE', 'Website'),
        ('ODT', 'Otago Daily Times'),
        ('OTHER', 'Other'),
    )

    # klass = models.ForeignKey('Klass', verbose_name='Class', on_delete=PROTECT)
    username = models.CharField(max_length=8, blank=True, null=True, verbose_name='Username')
    first_name = models.CharField(max_length=200, verbose_name='First Name', blank=True, null=True)
    surname = models.CharField(max_length=200, verbose_name='Last Name')
    # email = models.CharField(max_length=200, validators=[validate_email], blank=False, null=False)
    email = models.CharField(max_length=200, blank=False, null=False)
    phone = models.CharField(max_length=200, blank=False, null=True)
    mobile_phone = models.CharField(max_length=200, blank=False, null=True)
    clevel = models.CharField(max_length=10, blank=True, null=True)
    notes = models.CharField(max_length=2000, blank=True, null=True)
    seats_used = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(9999)])
    places_booked = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(9999)], verbose_name='Paying for Place')
    actual_course_cost = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, verbose_name='Amount')
    confirmed = models.BooleanField(default=False)
    # confirmed_date = models.DateField(blank=True, null=True)
    # charge_exported = models.BooleanField(default=False)
    # account_code = models.CharField(max_length=200, blank=True, null=True, verbose_name='Client Account Code')
    # charge_to_code = models.CharField(max_length=200, blank=True, null=True, verbose_name='Income Account Code')
    # invoice_code = models.CharField(max_length=200, blank=True, null=True, verbose_name='Purchase Order #')
    # payment_date = models.DateTimeField(blank=True, null=True)
    # cash_payment_receipt_number = models.CharField(max_length=50, blank=True, null=True, verbose_name='Cash Payment Receipt #')
    # payment_type = models.CharField(choices=KIND, max_length=50, verbose_name='Payment Type', blank=True, null=True)
    # copy_to_email = models.CharField(max_length=200, verbose_name='Copy to Email', validators=[validate_email], blank=True, null=True)
    # person_class = models.CharField(max_length=200, verbose_name='User Type', blank=False)
    # status = models.CharField(choices=statuses.BOOKING_STATUS, default=statuses.BOOKING_STATUS.PENDING, max_length=200, blank=True, null=False, verbose_name='Booking Status')
    # booking_type = models.CharField(choices=TYPE, max_length=200, blank=True, null=True)
    # person_jobs_dates_in_departments_pk = models.CharField(max_length=200, blank=True, null=True, verbose_name='Department')
    # booked_by_backend = models.BooleanField(default=False, blank=False, null=False)
    # booking_moved = models.BooleanField(default=False, blank=False, null=False)
    # how_heard_about = models.CharField(choices=HOW_HEARD_ABOUT, max_length=200, blank=True, null=True, verbose_name='How did you hear about this course?')

    def __str__(self):
        return '{0} {1}'.format(self.first_name, self.surname)

    # @property
    # def participant_name(self):
    #     return '{0} {1}'.format(self.first_name, self.surname)
    #
    # def save(self, *args, **kwargs):
    #     from training.engine.booking.participant_logic import ParticipantManager
    #     super(Booking, self).save(*args, **kwargs)
    #     ParticipantManager().add_participant_to_sessions(booking=self)
    #
    # def delete(self, using=None):
    #     super(Booking, self).delete(using=using)
    #
    # @staticmethod
    # def get_status_detail(value):
    #     for status in BOOKING_STATUS._doubles:
    #         code, human_readable = status
    #         if code == value:
    #             return human_readable
    #     return ''
    #
    # def department(self):
    #     department = PersonJobsDatesInDepartments.objects.using('hr-hr').filter(pk=self.person_jobs_dates_in_departments_pk)
    #     return department.first()
    #
    # @property
    # def apologies(self):
    #     from training.models.booking_session import BookingSession
    #     if self.klass.status == KLASS_STATUS_COMPLETED:
    #         if BookingSession.objects.attendance_count_by_id(booking=self.id) == BookingSession.objects.filter(booking=self).count():
    #             return ""
    #         if self.status != statuses.BOOKING_STATUS.COMPLETED:
    #             return ""
    #         if BookingSession.objects.apologies(booking=self):
    #             return '(Apologies)'
    #         return '(No Apologies)'
    #     return ""
    #
    class Meta:
        verbose_name = 'Booking'
        app_label = 'training'
    #
    # objects = BookingQuerySet.as_manager()


