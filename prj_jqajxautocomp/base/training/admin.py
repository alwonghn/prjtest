from django.contrib import admin

from training.models.participant import User

admin.site.register(User)
