# class CourseListJSONView(CourseListWithRetiredJSONView):
#
#     def get_queryset(self, tenant, query):
#         queryset = super().get_queryset(tenant, query)
#         return queryset.retired(retired='NO')


# class ParticipantListUsernameSearchJSONView(ManageMixin, JSONResponseMixin, View):
#
#     required_permissions = ('training.manage_courses', 'training.book_participants')
#
#     def get_content_type(self):
#         return 'application/javascript'
#
#     def get_queryset(self, query):
#         return User.objects.filter(username_contains=query)
#
#     def get(self, request, *args, **kwargs):
#         query_string = kwargs['query']
#         ##
#         print(query_string)
#         usernames = self.get_queryset(query_string)
#         return self.render_json_object_response(
#             usernames,
#             fields=(
#                 'id','username','first_name','last_name','is_active'
#             )
#         )
import json
from django.http import HttpResponse
from training.models.participant import User

def ParticipantListUsernameSearchJSONView(request):
    if request.is_ajax():
        search_term = request.GET.get('term','')

        print('TERM IS = {0}'.format(search_term))

        users = User.objects.filter(username__contains=search_term)
        usernames = []
        for user in users:
            user_json = {}
            user_json['username'] = user.username
            # user_json['first_name'] = user.first_name
            # user_json['last_name'] = user.last_name
            # user_json['email'] = user.email
            usernames.append(user_json)
        data = json.dumps(usernames)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

