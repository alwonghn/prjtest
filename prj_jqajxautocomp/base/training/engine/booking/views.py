from django.http import HttpResponse
from django.views.generic import TemplateView

from training.models.participant import Booking
from django.core.urlresolvers import reverse_lazy


# class AllParticipantList(ParticipantTopMenuMixin, ManageMixin, SingleTableMixin, SearchFilterListMixin):
class AllParticipantList(TemplateView):
    template_name = 'klass/all_participant_list.html'
    # table_class = AllParticipantListTable
    # table_pagination = {'per_page': RowsPerPageChoiceField.INITIAL_VALUE}
    # required_permissions = ('training.manage_courses', 'training.book_participants')
    # form_class = AllParticipantSearchForm
    success_url = reverse_lazy('admin_participant_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # kwargs.update({'tenant': self.request.user.tenant})
        return kwargs

    # def get_queryset(self):
    #     queryset = Booking.objects.tenant(tenant=self.request.user.tenant).prefetch_related('klass__course')
    #     if self.form.is_valid():
    #         # if self.form.cleaned_data['rows_per_page']:
    #         #     self.table_pagination = {'per_page': int(self.form.cleaned_data['rows_per_page'])}
    #         # if self.form.cleaned_data['confirmed']:
    #         #     queryset = queryset.confirmed(confirmed=self.form.cleaned_data['confirmed'])
    #         if self.form.cleaned_data['username']:
    #             queryset = queryset.username(username=self.form.cleaned_data['username'])
    #         # if self.form.cleaned_data['first_name']:
    #         #     queryset = queryset.first_name(first_name=self.form.cleaned_data['first_name'])
    #         # if self.form.cleaned_data['surname']:
    #         #     queryset = queryset.surname(surname=self.form.cleaned_data['surname'])
    #     queryset = queryset.list_optimization()
    #     queryset = queryset.add_sessions_column().add_attended_column()
    #     return queryset

    # def get_form_class(self):
    #     return AllParticipantSearchForm



