from training.models import Booking
from training.models.booking_session import BookingSession
from training.models.course import Session


class ParticipantManager():

    def add_participant_to_sessions(self, booking):
        sessions = Session.objects.for_booking(booking=booking)
        for session in sessions:
            if not BookingSession.objects.participant_in_session(session=session, booking=booking):
                booking_session = BookingSession(booking=booking, session=session)
                booking_session.save()

    def add_participants_to_sessions(self, klass):
        bookings = Booking.objects.participant_in(klass=klass)
        for booking in bookings:
            self.add_participant_to_sessions(booking=booking)