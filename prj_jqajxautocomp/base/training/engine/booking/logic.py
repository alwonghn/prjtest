from datetime import datetime
from django.db.models import Q, Sum, Count, Case, When, DecimalField, ExpressionWrapper


from django.db import models
from training.models import statuses
# from training.models.participant import Booking

# class BookingQuerySet(models.QuerySet):
#
#     def tenant(self, tenant):
#         return self.filter(klass__tenant=tenant)
#
#     def klass(self, klass):
#         return self.filter(klass=klass)
#
#     def first_name(self, first_name):
#         if not first_name:
#             return self.filter()
#         return self.filter(first_name__icontains=first_name)
#
#     def surname(self, surname):
#         if not surname:
#             return self.filter()
#         return self.filter(surname__icontains=surname)
#
#     def search_names(self, terms):
#         if not terms:
#             return self.filter()
#         queries = []
#         for term in terms.split():
#             queries.append(Q(first_name__icontains=term) | Q(surname__icontains=term) | Q(email__icontains=term))
#         filter_query = None
#         for query in queries:
#             if filter_query is None:
#                 filter_query = query
#             else:
#                 filter_query &= query
#         return self.filter(filter_query)
#
#     def confirmed(self, confirmed):
#         if confirmed == 'YES':
#             return self.filter(confirmed=True)
#         elif confirmed == 'NO':
#             return self.filter(confirmed=False)
#         else:
#             return self.filter()
#
#     def has_seats_used(self):
#         return self.filter(seats_used__gt=0)
#
#     def participant_in(self, klass):
#         return self.filter(klass=klass)
#
#     def confirmed_count_for_klass(self, klass):
#         return self.filter(klass=klass).confirmed('YES').count()
#
#     def booking_count_for_klass(self, klass):
#         return self.filter(klass=klass, seats_used__gt=0).exclude(status=statuses.BOOKING_STATUS.WITHDRAWN).aggregate(Sum('seats_used'))
#
#     def with_username(self, username):
#         if username:
#             return self.filter(username=username)
#         return self.filter()
#
#     def has_status(self, status):
#         return self.filter(status=status)
#
#     def not_withdrawn(self):
#         return self.exclude(status=statuses.BOOKING_STATUS.WITHDRAWN)
#
#     def booking_completed(self):
#         return self.filter(status=statuses.BOOKING_STATUS.COMPLETED)
#
#     def to_be_exported(self, tenant):
#
#         result = self.filter(klass__tenant=tenant, charge_exported=False, klass__status=statuses.KLASS_STATUS_COMPLETED, klass__first_session__day__lte=datetime.today())
#         result = result.filter(payment_type=Booking.KIND.invoice, actual_course_cost__gt=0, places_booked__gt=0, status=statuses.BOOKING_STATUS.COMPLETED, confirmed=True)
#         result = result.order_by('klass__first_session__day')
#         return result
#
#     def attended(self, booking):
#         # attended greater than 50% of sessions gets evaluations
#         total_sessions = self.get(id=booking.id).bookingsession_set.all().count()
#         attended_sessions = self.filter(id=booking.id).filter(bookingsession__attended=True).count()
#         return attended_sessions / total_sessions > 0.5
#
#     def list_optimization(self):
#         return self.select_related('klass__first_session', 'klass')
#
#     def year(self, year):
#         return self.filter(klass__first_session__day__year=year)
#
#     def charge_is_exported(self, is_exported):
#         return self.filter(charge_exported=is_exported)
#
#     def add_attended_column(self):
#         return self.annotate(attended=Count(Case(When(bookingsession__attended=True, then=1))))
#
#     def add_fraction_attended_column(self):
#         return self.annotate(fraction_attended=ExpressionWrapper(Count(Case(When(bookingsession__attended=True, then=1)))/Count('bookingsession'), output_field=DecimalField()))
#
#     def add_sessions_column(self):
#         return self.annotate(sessions=Count('bookingsession'))
#
#     def starts_on(self, day):
#         return self.filter(klass__first_session__day=day)
#
#     def klass_status(self, status):
#         return self.filter(klass__status=status)
#
#     def session_on(self, day):
#         return self.filter(bookingsession__session__day=day)





# class BookingSessionQuerySet(models.QuerySet):
#
#     def tenant(self, tenant):
#         return self.filter(booking__klass__tenant=tenant)
#
#     def attendance_count_by_id(self, booking):
#         return self.filter(booking__id=booking, attended=True).count()
#
#     def participant_in_session(self, booking, session):
#         return self.filter(session=session, booking=booking).exists()
#
#     def booking(self, booking):
#         if booking:
#             return self.filter(booking=booking)
#         return self.filter()
#
#     def for_mark_attendance(self, session):
#         return self.filter(session=session).filter(booking__status=statuses.BOOKING_STATUS.COMPLETED).order_by('booking__surname')
#
#     def attended(self):
#         return self.filter(attended=True)
#
#     def apologies(self, booking):
#         return self.filter(booking=booking, apologies=True).exists()