
from django.conf.urls import url
from training.engine.booking.views import AllParticipantList
from training.engine.course.views import ParticipantListUsernameSearchJSONView

urlpatterns = [

    # Participant
    url(r'^participant/list/$', AllParticipantList.as_view(), name='admin_participant_list'),

    #Ajax
    # url(r'^participant/list/university-user/search-username/(?P<query>.+)/$', ParticipantListUsernameSearchJSONView.as_view(), name='participant_list_search_username_json'),
    url(r'^participant/list/university-user/search-username/$', ParticipantListUsernameSearchJSONView,
        name='participant_list_search_username_json'),
]

